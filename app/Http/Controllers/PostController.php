<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;

class PostController extends Controller
{

    //OBTENER TODOS MIS POSTS
    public function getMisPosts(Request $request)
    {
        $this->middleware('auth');

        $page = isset($request->page) ? $request->page : 1;

        $posts = new Post();
        $posts = $posts->getPostByUserId(Auth::user()->id);
        $posts = $this->paginate($posts, 6, $page); //PAGINACIÓN
        $posts = $posts->withPath('mis-posts'); //CAMBIAR LA URL DE LA PAGINACIÓN

        return view("posts.ver-posts", [
            "posts" => $posts,
            "title_page" => "Mis posts"
        ]);
    }

    //BUSCAR POSTS
    public function postBuscarPosts(Request $request)
    {
        $busqueda = $request->search;
        $page = isset($request->page) ? $request->page : 1;

        $posts = new Post();
        $posts = $posts->getBusquedaPosts($busqueda);
        $posts = $this->paginate($posts, 6, $page); //PAGINACIÓN
        $posts = $posts->withPath('posts'); //CAMBIAR LA URL DE LA PAGINACIÓN

        return view("posts.ver-posts", [
            "posts" => $posts,
            "title_page" => "Resultados para '$busqueda'",
            "search" => $busqueda
        ]);
    }

    //OBTENER POSTS POR AUTOR
    public function getPostsByAuthor(Request $request, $id_author)
    {

        //SI ESTÁ LOGUEADO Y LA PÁGINA DEL AUTOR QUE VA A VISITAR
        //ES LA SUYA, REDIRECCIONA A /mis-posts
        if (Auth::check() && Auth::user()->id == $id_author){
            return redirect(route('get.mis.posts'));
        }

        $page = isset($request->page) ? $request->page : 1;

        $posts = new Post();
        $posts = $posts->getPostByUserId($id_author);
        $posts = $this->paginate($posts, 6, $page); //PAGINACIÓN
        $posts = $posts->withPath("$id_author"); //CAMBIAR LA URL DE LA PAGINACIÓN

        return view("posts.ver-posts", [
            "posts" => $posts,
            "title_page" => "Autor $id_author"
        ]);
    }

    //OBTENER TODOS LOS POSTS
    public function getPosts(Request $request)
    {
        $page = isset($request->page) ? $request->page : 1;

        $posts = new Post();
        $posts = $posts->getPosts();
        $posts = $this->paginate($posts, 6, $page); //PAGINACIÓN
        $posts = $posts->withPath('posts'); //CAMBIAR LA URL DE LA PAGINACIÓN

        return view("posts.ver-posts", [
            "posts" => $posts,
        ]);
    }

    public function getPost($post_id = null)
    {
        $posts = new Post();
        $post = $posts->getPostById($post_id);

        //REDIRECCIONAMOS SI EL POST NO EXISTE
        if ($post == null) {
            return redirect('/404');
        }

        return view("posts.ver-post", [
            "post" => $post
        ]);
    }

    //PAGINAR ARRAYS
    public function paginate($items, $perPage = 5, $page = null, $options = [])
    {
        $page = $page ?: (Paginator::resolveCurrentPage() ?: 1);
        $items = $items instanceof Collection ? $items : Collection::make($items);
        return new LengthAwarePaginator($items->forPage($page, $perPage), $items->count(), $perPage, $page, $options);
    }

}
