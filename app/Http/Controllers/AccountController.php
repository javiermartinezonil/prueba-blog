<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AccountController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getMiCuenta()
    {
        return view('account.ver-cuenta');
    }

    //CERRAR SESIÓN
    public function getLogout(){
        if (Auth::check()) {
            Auth::logout();
        }
        return redirect(route('login'));
    }

    public function postCambiarContrasena(Request  $request){
        $this->validate($request, [
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);
        //TODO CAMBIAR IDIOMA DE LAS RESPUESTAS DE VALIDACIÓN

        $user = Auth::user();
        $user->password = bcrypt($request->password);
        $user->save();
        return back();
    }

    //GENERAR O REGENERAR LA CLAVE API DEL USUARIO
    public function getGenerarAPI()
    {
        //CREAMOS EL TOKEN
        $user = \Auth::user();
        $token = $user->createToken('IPGlobal');
        $user->api = $token->accessToken;
        $user->save();

        return redirect(route('get.mi-cuenta'));
    }
}
