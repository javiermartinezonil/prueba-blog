<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/**
 * @OA\Info(
 *      version="1.0.0",
 *      title="POSTS",
 *      description="Lista de endpoints para los POSTS",
 * ),
 * @OA\SecurityScheme(
 *    securityScheme="bearerAuth",
 *    in="header",
 *    name="bearerAuth",
 *    type="http",
 *    scheme="bearer",
 *    bearerFormat="JWT",
 * ),
 */
class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     * Mostramos el listado de los regitros solicitados.
     * @return \Illuminate\Http\JsonResponse
     * @OA\Get(
     *     path="/api/posts",
     *     tags={"POSTS"},
     *     summary="Devuelve todos los posts incluyendo la información del usuario",
     *     description="Debes usar en el encabezado Bearer API_TOKEN",
     *     security={
     *          {"bearerAuth":{}},
     *      },
     *     @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         @OA\Schema(
     *              type="string",
     *              default="XMLHttpRequest",
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de posts"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Error de servidor"
     *     )
     * )
     */
    //DEVUELVE LOS POSTS
    public function getPosts()
    {
        $post = new Post();
        try {
            $posts = $post->getPostsAndUserInfo();
        } catch (\Exception $exception) {
            //DEVOLVEMOS EL ERROR
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($posts, 200);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     *
     * @OA\Post(
     *     path="/api/new-post",
     *     tags={"POSTS"},
     *     summary="Endpoint para crear un publicar un nuevo post",
     *     description="Debes usar en el encabezado Bearer API_TOKEN",
     *     security={
     *          {"bearerAuth":{}},
     *      },
     *     @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         @OA\Schema(
     *              type="string",
     *              default="XMLHttpRequest",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="title",
     *         in="query",
     *         description="Título del post",
     *         required=true,
     *      ),
     *     @OA\Parameter(
     *         name="body",
     *         in="query",
     *         description="Descripción del post",
     *         required=true,
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Devuelve un JSON del post publicado"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Error de servidor"
     *     )
     * )
     */
    public function postNewPost(Request $request)
    {
        //VALIDACIÓN DE LOS CAMPOS
        $request->validate([
            'title' => 'required|string',
            'body' => 'required|string'
        ]);

        $title = $request->title;
        $body = $request->body;

        try {
            //TODO AGREGAR POST A LA BD
            $post = new Post();
//            $post->title = $title;
//            $post->body = $body;
//            $post->userId = Auth::user()->id;
//            $post->save();

        } catch (\Exception $exception) {
            //DEVOLVEMOS EL ERROR
            return response()->json($exception->getMessage(), 500);
        }

        $response = [
            "message" => "Post posted successfully",
            "id" => rand(100, 200), //TODO AQUÍ PONDRÍAMOS EL ID DEVUELTO DE LA BD,
            "title" => $title,
            "body" => $body
        ];
        return response()->json($response, 200);

    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     * @OA\Get(
     *     path="/api/search-posts",
     *     tags={"POSTS"},
     *     summary="Devuelve los posts que coincidan con la búsqueda.",
     *     description="Debes usar en el encabezado Bearer API_TOKEN",
     *     security={
     *          {"bearerAuth":{}},
     *      },
     *     @OA\Parameter(
     *         name="X-Requested-With",
     *         in="header",
     *         @OA\Schema(
     *              type="string",
     *              default="XMLHttpRequest",
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="busqueda",
     *         in="query",
     *         description="Palabra o frase a buscar en los posts",
     *         required=true,
     *      ),
     *     @OA\Response(
     *         response=200,
     *         description="Lista de posts buscados"
     *     ),
     *     @OA\Response(
     *         response="500",
     *         description="Error de servidor"
     *     )
     * )
     */
    //DEVUELVE LOS POSTS
    public function getBusqueda(Request $request)
    {
        //VALIDACIÓN DE LOS CAMPOS
        $request->validate([
            'busqueda' => 'required|string|min:4',
        ]);

        $posts = new Post();
        try {
            $posts = $posts->getBusquedaPosts($request->busqueda);
        } catch (\Exception $exception) {
            //DEVOLVEMOS EL ERROR
            return response()->json($exception->getMessage(), 500);
        }

        return response()->json($posts, 200);
    }
}
