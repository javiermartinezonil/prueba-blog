<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/cerrar-sesion', [App\Http\Controllers\AccountController::class, 'getLogout'])->name('logout');
Route::get('/mi-cuenta', [App\Http\Controllers\AccountController::class, 'getMiCuenta'])->name('get.mi-cuenta');
Route::post('/cambiar-contrasena', [App\Http\Controllers\AccountController::class, 'postCambiarContrasena'])->name('post.cambiar.contrasena');

Route::post('/buscar', [App\Http\Controllers\PostController::class, 'postBuscarPosts'])->name('post.buscar');
Route::get('/generar-api', [App\Http\Controllers\AccountController::class, 'getGenerarAPI'])->name('get.generar-api');
Route::get('/posts', [App\Http\Controllers\PostController::class, 'getPosts'])->name('get.posts');
Route::get('/mis-posts', [App\Http\Controllers\PostController::class, 'getMisPosts'])->name('get.mis.posts');
Route::get('/ver-post/{post_id}', [App\Http\Controllers\PostController::class, 'getPost'])->name('get.post');
Route::get('/ver-posts-autor/{id_author}', [App\Http\Controllers\PostController::class, 'getPostsByAuthor'])->name('get.posts.author');
