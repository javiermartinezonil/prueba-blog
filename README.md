#Información sobre la prueba
La prueba está desarrollada con Laravel. Como base de datos he usado MySQL para la gestión de usuarios y roles, pero
los posts están almacenados en [https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/)
para agilizar el desarrollo de la prueba.

##Intrucciones
Una vez desplegado el proyecto podemos ir a registrarnos desde el menú, al crear el usuario se crea
un id al cual lo asocio a los posts de jsonplaceholder, es decir, el usuario con id 1 tendrá los posts con userId 1.
<br><br>
Podemos navegar entre los posts (las páginas tienen paginación de array ya que no uso Eloquent), abrirlos para más información, etc...
También podemos buscar posts, ver posts de un autor con su ID (/ver-posts-autor/1)
<br><br>
En la pestaña de Mi cuenta podemos ver el perfil del usuario, cambiar la contraseña y generar nuestra
clave API la cual nos servirá para más tarde.
<br><br>
He creado una tabla para los roles, existen 2 tipos de roles Usuario y Administrador, desde aquí el usuario administrador
podría eliminar y editar los posts que quiera.


## Composer
Paquetes instalados con composer:

- Laravel/UI - Plantilla de login
- Passport - API
- darkaonline/l5-swagger - Paquete de composer de Swagger

## API
Finalmente la documentación de la API la he hecho con SwaggerUI, se pueden ver los endpoints en:
[http://blog.test/api/documentation](/http://blog.test/api/documentation)
<br>
Para poder probar la API tendremos que usar la API generada anteriormente y los headers necesarios que aparecen en la documentación.

## Conclusión
En total la prueba me ha llevado sobre 8-9 horas, ya que he tenido algún problema:
- Me he atascado en la API porque el autocompletar de PHPStorm me ha importado una librería que no tocaba, funcionaba todo bien solo que al crear el token me devolvía un objeto y me ha costado darme cuenta
