<?php

namespace Tests\Unit;

use Tests\TestCase;

class PostsTest extends TestCase
{
    private const API = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiI5NmE4OTA3Zi1iNWNiLTQyOTctOTljMi0wZTY1OTQ1YTcyMGMiLCJqdGkiOiI1YmRlNjU5ZmY4ODg0ZDU1NDkxYzU1NWVkOTcyYzQzZTE3NmRiODNkZTI2MTUxOTA2ZTZkN2YwMjY5Zjg5MTAzNmVkNzVkZDVlY2M5ODZmNiIsImlhdCI6MTY1NjUwNzIzMS4yNzQ1MzgsIm5iZiI6MTY1NjUwNzIzMS4yNzQ1NDcsImV4cCI6MTY4ODA0MzIzMS4yNjM3NTgsInN1YiI6IjEiLCJzY29wZXMiOltdfQ.ijtSfsx0WcDpHp3T_VI1H5DdNeMJIBbkDrBVD74S1AMZoy53D7VXHqHA0Zkv8e0MltkJ5aAIO-cNpZopJWL0yzDJR_HoT874V7k0Cj4RraX3jE5ZouyiZSwOEmAvn4ZT-BoscRn3xO_EzC9S9sFddPmUbLNSI2dl6EPdnPtT7joY1xTIAaqYHrn0sdQ8kohPEZ2Y2FmuBpSw6LboZwuJ1x-hQPNB5VspIK7bASKLRlbJVjWab9GQhK7unke-nmtgNu08vKaFmSZVnFc74PW0yvjyUqExw0ConwqwogDVZnHrP-FuHTpKic4npnuQEvdMAnXtXoIk9kEIVIzHUw2WwSm_9jzEMDBxZgxgme9WZot_vfKN0E2EPDJoqVr3fgUUnqEQ32jng3brh5ODmtkZlJ9VdSdOOe47jCV3uj2QhLiaoTORlOqtxC0WWTPSl-1Lhby7Eh283B6FMSrUo_B8EOpCFz8_se7gx7gGRL20jhenq2Wu9SjFhQuH9tFDL69pn0mmJXCZ_mDhT9ALDZq5J53xhIrx8wnJ_yDt_aqtiAoSuDFGUtry5Lqfz2u7C40iQXpTBJurzGKiF4_ZRhUXfZTE7cki9oF-5DfEDnm9uG7EK6GnbaR_6Dngj1MPaL51_srw1I29mFkzjdomceWockJsOaWPEug03blsGTHFsTU";

    /** @test */
    public function get_posts()
    {
        //GET POSTS
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . self::API,
        ])->json('GET', '/api/posts');

//        $response->dd();
        $response->assertStatus(200);
    }

    /** @test */
    public function new_post()
    {
        //NEW POSTS
        $response = $this->withHeaders([
            'X-Requested-With' => 'XMLHttpRequest',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer ' . self::API,
        ])->json('POST', '/api/new-posts', [
            "title" => "TITULO",
            "body" => "DESCRIPCÓN"
        ]);

        $response->assertStatus(200);
    }
}
