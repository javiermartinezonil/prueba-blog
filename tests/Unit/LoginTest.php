<?php

namespace Tests\Unit;


use Tests\TestCase;

class LoginTest extends TestCase
{

    /** @test */
    public function authenticated_to_a_user()
    {
        $credentials = [
            "email" => "usuario@gmail.com",
            "password" => "12345678"
        ];

        $response = $this->post('/login', $credentials);
        $this->assertCredentials($credentials);
    }

    /** @test */
    public function error_authenticated_to_a_user()
    {
//        $this->withoutExceptionHandling();

        $credentials = [
            "email" => "usuario@gmail.com",
            "password" => "123456789"
        ];

        $response = $this->post('/login', $credentials);
        $this->assertInvalidCredentials($credentials);
    }

}
