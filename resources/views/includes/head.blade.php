
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{ config('app.name', 'Laravel') }}</title>

<!-- Bootstrap Core CSS -->
{{--<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" crossorigin="anonymous">--}}

<link rel="stylesheet" href="{!! asset('css/bootstrap.min.css') !!}">
<link rel="stylesheet" href="{!! asset('css/bootstrap-grid.min.css') !!}">
<link rel="stylesheet" href="{!! asset('css/bootstrap-reboot.min.css') !!}">
<link rel="stylesheet" href="{!! asset('css/blog.css') !!}">
<link rel="stylesheet" href="{!! asset('css/theme.css') !!}">

<link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">
