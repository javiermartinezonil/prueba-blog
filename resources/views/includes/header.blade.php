<section class="jumbotron text-center">
    <div class="container">
        <h1 class="jumbotron-heading">{{env('APP_NAME')}}</h1>
        <p class="lead text-muted">¿Quieres buscar un post?</p>
        <form class="input-group" action="{{route('post.buscar')}}" method="POST">
            @csrf
            @method('POST')
            <input type="search" class="form-control rounded" value="{{$search ?? ''}}" placeholder="Introduce aquí tu búsqueda" aria-label="Search" name="search" />
            <button type="submit" class="btn btn-outline-primary">Buscar</button>
        </form>
    </div>
</section>
