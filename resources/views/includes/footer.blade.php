<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Volver arriba</a>
        </p>
        <p>{{env('APP_NAME')}} &copy; Bootstrap Theme Example</p>
    </div>
</footer>
