<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <br><br>
   <div>
       <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
           <span class="navbar-toggler-icon"></span>
       </button>
       <a class="navbar-brand title-page" href="/">{{env('APP_NAME')}}</a>
   </div>
    <div class="collapse navbar-collapse menu-center" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item {{ isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] == '/' ? 'active':'' }}">
                <a class="nav-link" href="/">Inicio</a>
            </li>
            <li class="nav-item">
                <a class="nav-link {{ isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] == '/posts' ? 'active':'' }}" href="{{route('get.posts')}}">Posts</a>
            </li>
            @if(!\Illuminate\Support\Facades\Auth::check())
                <li class="nav-item">
                    <a class="nav-link {{ isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] == '/login' ? 'active':'' }}" href="{{route('login')}}">Iniciar sesión</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] == '/register' ? 'active':'' }}" href="{{route('register')}}">Registrarse</a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link {{ isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] == '/mis-posts' ? 'active':'' }}" href="{{route('get.mis.posts')}}">Mis posts</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ isset($_SERVER["REQUEST_URI"]) && $_SERVER["REQUEST_URI"] == '/mi-cuenta' ? 'active':'' }}" href="{{route('get.mi-cuenta')}}">Mi cuenta</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('logout')}}">Cerrar sesión</a>
                </li>
            @endif
        </ul>
    </div>
</nav>

