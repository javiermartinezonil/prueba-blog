@extends('layouts.default')
@section('content')

    <section class="jumbotron text-center"
             style="width: 100%;background: no-repeat url('https://picsum.photos/800');background-size: 100%">
        <div class="container">
            <h1 class="jumbotron-heading">{{strlen($post->title) > 15 ? substr($post->title,0,15).' ...' : $post->title}}</h1>
        </div>
    </section>

    <div class="blog-post">
        <h2 class="blog-post-title">{{$post->title}}</h2>
        <p class="blog-post-meta">Junio 28, 2022 <a href="{{route('get.posts.author', $post->userId)}}">Autor {{$post->userId}}</a></p>

        <p>{{$post->body}}</p>
    </div>


@endsection
