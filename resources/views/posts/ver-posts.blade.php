@extends('layouts.home')
@section('content')


    <h1>{{$title_page ?? 'Todos los posts'}}</h1>
    <div class="row mb-2">
        @foreach($posts as $post)

            <div class="col-md-6">
                <div class="card flex-md-row mb-4 box-shadow h-md-250">
                    <div class="card-body d-flex flex-column align-items-start">
                        <strong class="d-inline-block mb-2 text-primary">Categoría {{rand(1,9)}}</strong>
                        <h3 class="mb-0">
                            <a class="text-dark"
                               href="{{route('get.post', $post->id)}}">{{strlen($post->title) > 15 ? substr($post->title,0,15).' ...' : $post->title}}</a>
                        </h3>
                        <div class="mb-1 text-muted">Jun {{rand(1,30)}}</div>
                        <p class="card-text mb-auto">{{strlen($post->body) > 90 ? substr($post->body,0,90).' ...' : $post->title}}</p>
                        @if(\Illuminate\Support\Facades\Auth::check() && (\Illuminate\Support\Facades\Auth::user()->hasRole('admin') || \Illuminate\Support\Facades\Auth::user()->id == $post->userId))
{{--                            TODO CREAR EDITAR Y ELIMINAR--}}
                            <a href="#editar">Editar</a>
                            <a href="#eliminar">Eliminar</a>
                        @endif
                    </div>
                    <img class="card-img-right flex-auto d-none d-md-block" alt="Portada"
                         style="width: 200px; height: 250px;" src="https://picsum.photos/30{{rand(0,9)}}"
                         data-holder-rendered="true">
                </div>
            </div>
        @endforeach

    </div>
    @if(count($posts) > 0)
        <div class="paginate-links">
            {!! $posts->links() !!}
        </div>
    @else
        <div class="alert alert-danger col-12" role="alert">
            No existen posts públicos.
        </div>
    @endif
@endsection
