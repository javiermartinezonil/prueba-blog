@extends('layouts.default')
@section('content')

    <section id="account">
        <div class="row">
            <div class="col-xl-3 col-lg-12">
                <div class="card mb-4">
                    <div class="card-header">
                        <div class="card-title-wrap bar-primary">
                            <div class="card-title">Información personal</div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="card-block">
                            <div class="align-self-center halfway-fab text-center">
                                <a class="profile-image">
                                    <img src="{{asset('imgs/users/user.JPG')}}"
                                         class="rounded-circle img-border gradient-summer img-profile"
                                         alt="Imagen de perfil">
                                </a>
                            </div>
                            <div class="text-center">
                                <span class="font-medium-2">{{\Illuminate\Support\Facades\Auth::user()->name}}</span>
                                <p class="grey font-small-2">{{\Illuminate\Support\Facades\Auth::user()->email}}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-9 col-lg-12">
                <div id="about">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title-wrap bar-warning">Cambiar contraseña
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-block">
                                        <form action="{{route('post.cambiar.contrasena')}}" method="POST">
                                            @csrf
                                            @method('POST')
                                            <div class="form-group">
                                                <label for="pass1">Contraseña</label>
                                                <input type="password" class="form-control" name="password" min="6"
                                                       required
                                                       id="pass1" placeholder="Escribe la nueva contraseña">
                                            </div>
                                            <div class="form-group">
                                                <label for="pass1">Repite la contraseña</label>
                                                <input type="password" class="form-control" name="password_confirmation"
                                                       min="6" required
                                                       id="pass2" placeholder="Repite la nueva contraseña">
                                            </div>
                                            <button type="submit" class="btn btn-outline-success btn-lg">Cambiar
                                                contraseña
                                            </button>

                                            @if($errors->any())
                                                <br><br>
                                                <div class="alert alert-danger col-12" role="alert">
                                                    <ul>
                                                        @foreach($errors->all() as $error)
                                                            <li style="color: darkred"> {{ $error }} </li>
                                                        @endforeach
                                                    </ul>
                                                </div>

                                            @endif
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <div class="card-title-wrap bar-info">
                                        <div class="card-title">Claves API</div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <div class="card-block">
                                        @if(!isset(\Illuminate\Support\Facades\Auth::user()->api))
                                            <a href="{{route('get.generar-api')}}"
                                               class="btn btn-outline-danger btn-lg">Generar clave API</a>
                                        @else
                                            <div class="input-group mb-3">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text" id="basic-addon1">Bearer</span>
                                                </div>
                                                <input type="text" class="form-control" id="input_api_token"
                                                       value="{{\Illuminate\Support\Facades\Auth::user()->api}}"
                                                       readonly>
                                            </div>
                                            <button onclick="copyToClipBoard()" class="btn btn-outline-primary btn-lg">
                                                Copiar clave API
                                            </button>
                                            <br><br>
                                            <a href="{{route('get.generar-api')}}"
                                               class="btn btn-outline-danger btn-lg">Regenerar clave API</a>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <script>
        function copyToClipBoard() {
            let content = document.getElementById('input_api_token');
            content.focus();
            content.select();
            return document.execCommand('copy');
        }
    </script>

@endsection
