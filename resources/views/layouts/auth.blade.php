<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.head')
</head>
<body>
    @include('includes.nav')
    @yield('content')
    @include('includes.footer')
    @include('includes.footer-scripts')
</body>
</html>
