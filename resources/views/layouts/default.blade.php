<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    @include('includes.head')
</head>
<body>
    @include('includes.nav')
    <div class="contenedor-margin row">
        @yield('content')
    </div>
    @include('includes.footer')
    @include('includes.footer-scripts')
</body>
</html>
